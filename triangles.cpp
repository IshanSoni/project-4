/*
 * CSc103 Project 4: Triangles
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References: Stackoverflow for error fixing.
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: ~1
 */

#include "triangles.h" 
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;

unsigned long triangle::perimeter() {
	return s1+s2+s3;
}

unsigned long triangle::area() {
    unsigned long sides[3] = {s1,s2,s3};
	sort(sides,sides+3);
	return (sides[0]*sides[1])/2;
}

void triangle::print() {
	cout << "[" << s1 << "," << s2 << "," << s3 << "]";
}

bool congruent(triangle t1, triangle t2) {
	unsigned long s1[3] = {t1.s1,t1.s2,t1.s3};
	unsigned long s2[3] = {t2.s1,t2.s2,t2.s3};
	sort(s1,s1+3);
	sort(s2,s2+3);
	if(s1[0] == s2[0] && s1[1] == s2[1] && s1[2] == s2[2])
	   return true;
	return false;
}

bool similar(triangle t1, triangle t2) {
	double s1[3] = {t1.s1,t1.s2,t1.s3};
	double s2[3] = {t2.s1,t2.s2,t2.s3};
	sort(s1,s1+3);
	sort(s2,s2+3);
	if(s1[0]/s2[0] == s1[1]/s2[1] && s1[0]/s2[0] == s1[2]/s2[2])   
	   return true;
	return false;
}

vector<triangle> findRightTriangles(unsigned long l, unsigned long h) {
	vector<triangle> retval; // storage for return value.
	for(int i = 1; i < h; i++)
	   for(int j = i+1; j < h; j++)
	      for(int k = j+1; k < h; k++)
		     if(i*i + j*j == k*k && i+j+k <= h && i+j+k >= l)
			    retval.push_back(triangle(i, j, k));   
	return retval; 
}

