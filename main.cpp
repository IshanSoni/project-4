#include "triangles.h"
#include <iostream>
#include <cmath>
using std::cout;
using std::endl;

int main(void) {
	// TODO: write some code to test out your program.
	// This won't be graded- it is just for your benefit
	// and to aid your testing / debugging.
	triangle T(4,5,3);
	triangle R(5,3,4);
	cout << T.area() << endl;
	cout << congruent(T, R) << endl;
	cout << similar(T, R) << endl;
    vector<triangle> test = findRightTriangles(20, 40);
	for(int i = 0; i < test.size(); i++)
	{
	   test[i].print();
	} 
 	
			  
    return 0;
	
}
